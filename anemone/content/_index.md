+++
+++

## Overview

Hi, here is my personal website for Duke IDS721 Cloud Computing instructed by Prof. [Noah Gift](https://noahgift.com). This website is built with [Zola](https://www.getzola.org/) and adapted from [Anemone](https://www.getzola.org/themes/anemone/).

> *"Useless blockquote"*

## Blog Posts

Explore my insightful IDS721 blog posts on a variety of topics:

- 🥣 [Overview Post](./blog/overview-post)
- 📺 [Week1-Mini-Project](./blog/week1-mini-project)

## Tags

Browse our posts by tags:

- [example](./tags/example)

## Another List

Discover additional content:

- With Subitems
  - With Subsubitems
  - [Example Page](./about)
- this list is just the content of `content/_index.md`, the tests are shamelessly stolen from [no style, please!](https://www.getzola.org/themes/no-style-please/)

## Online Presence

Stay connected with us:

- Email: [yyl2322@iCloud.com](mailto:yyl2322@iCloud.com)
- Code Repositories: [CR72322@GitHub](https://github.com/CR72322?tab=repositories)

## Webrings

Join our webrings and explore more:

- 🈯 {{ webring(prev="#", webring="#", webringName="Random Webring", next="#") }}
- 🎶 {{ webring(prev="#", webring="#", webringName="Another Webring", next="#") }}