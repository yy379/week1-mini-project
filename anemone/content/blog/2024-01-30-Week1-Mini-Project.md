+++
title = "Week1 Mini-Project"
[taxonomies]
  tags = ["example"]
[extra]
  toc = true
+++

[![Clippy](https://github.com/nogibjj/rust-data-engineering/actions/workflows/lint.yml/badge.svg)](https://github.com/nogibjj/rust-data-engineering/actions/workflows/lint.yml)
[![Tests](https://github.com/nogibjj/rust-data-engineering/actions/workflows/tests.yml/badge.svg)](https://github.com/nogibjj/rust-data-engineering/actions/workflows/tests.yml)


# Week1 Mini-Project

## Project Description

This project is about creating a static site with [zola](https://www.getzola.org) and deploying it to [Gitlab](https://gitlab.com/yy379/week1-mini-project). This site will hold all of the portofolio work in IDS721 Cloud Computing class.

## Deploy the Site

1. Install zola (Details in the [Install Zola](https://www.getzola.org/documentation/getting-started/installation/) section)
```bash
$ brew install zola
```
2. Clone the repository
```bash
$ git clone
```
3. Change the directory
```bash
$ cd anemone
```
4. Run the site locally
```bash
$ zola serve
```



## References

* [Getting Started with Zola](https://www.getzola.org/documentation/getting-started/overview/)
* [GitHub Copilot CLI](https://www.npmjs.com/package/@githubnext/github-copilot-cli)
* [Rust Fundamentals](https://github.com/alfredodeza/rust-fundamentals)
* [Rust Tutorial](https://nogibjj.github.io/rust-tutorial/)
* [Rust MLOps Template](https://github.com/nogibjj/mlops-template)
